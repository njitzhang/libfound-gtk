# libfound-gtk
libfound-gtk3 contains the libfound-gtk3 library as well as a data
file that exports the "FOUND" version.

The libfound-gtk3 library provides API shared by GTK+3.0 applications
on the desktop. Documentation for the API is available with
gtk-doc.


## 安装

### 构建过程

1. 确保已安装所有依赖库.

*不同发行版的软件包名称可能不同，如果您的发行版提供了 libfound-gtk，请检查发行版提供的打包脚本。*

如果你使用的是 [Centos] 或者其它提供了 libfound-gtk 的发行版:

```
$ sudo yum-builddep -y libfound-gtk3.spec
```

2. 构建:
```
$ rpmbuild -ba libfound-gtk3.spec
```

3. 安装:

```
$ sudo rpm -Uvh --force libfound-gtk*.rpm
```

## 帮助



## 开源许可证
libfound-gtk is distributed under the terms of the GNU General Public License, version 2 or later. See the COPYING file for details.

